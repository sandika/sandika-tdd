from django.shortcuts import render
from status_form.models import VisitorStatus
from status_form.forms import StatusForm

# Create your views here.
def index(request):
	form = StatusForm()
	visitor_status = VisitorStatus.objects.all()
	return render(request, 'index.html', {'form' : form, 'visitor_status' : visitor_status})