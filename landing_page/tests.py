from django.test import TestCase, RequestFactory

from .views import *

class IndexViewTestCase(TestCase):
	def setUp(self):
		self.factory = RequestFactory()

	def test_index_view_use_correct_template(self):
		"""
		Test that index view returns 200
		and use the correct template
		"""
		request = self.factory.get('/')
		response = index(request)
		self.assertEqual(response.status_code, 200)

		"""
		Test that the index function renders the
		correct template
		"""
		with self.assertTemplateUsed('index.html'):
			response = index(request)
			self.assertEqual(response.status_code, 200)