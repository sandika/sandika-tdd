from django.test import TestCase, RequestFactory, Client
from django.urls import resolve

from .views import *
from .models import *

class FormURLsTestCase(TestCase):
	def test_submit_form_will_fire_submit_form_view(self):
		"""
		Test that the ./submit-form/ resolves
		to the correct view function (submit_form)
		"""
		root = resolve('/submit-form/')
		self.assertEqual(root.func, submit_form)

class FormModelsTestCase(TestCase):
	def test_model_can_add_new_status(self):
		"""
		Test to make sure we can add an object to
		the models.
		"""
		new_status = VisitorStatus.objects.create(status="Test input status.")
		self.assertEqual(VisitorStatus.objects.all().count(), 1)

class FormViewsTestCase(TestCase):
	def test_submit_form_uses_submit_form_function(self):
		"""
		Test that submit_form function is used when accessing /submit-form/
		"""
		found = resolve('/submit-form/')
		self.assertEqual(found.func, submit_form)

	def test_submit_form_success_render_result(self):
		"""
		Test to make sure the status is rendered when it is
		successfully posted (not empty), and an object is created in the models.
		"""
		status = "Coba coba"
		response_post = Client().post('/submit-form/', {'status' : status})

		response = Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn(status, html_response)

		self.assertEqual(VisitorStatus.objects.all().count(), 1)


