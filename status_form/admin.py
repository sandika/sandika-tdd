from django.contrib import admin
from .models import VisitorStatus

# Register your models here.
admin.site.register(VisitorStatus)