from django import forms
from .models import VisitorStatus

class StatusForm(forms.ModelForm):
	class Meta:
		model = VisitorStatus
		fields = ['status']
	status = forms.CharField(label='Status', max_length=300)