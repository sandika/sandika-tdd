from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import VisitorStatus
from .forms import StatusForm

def submit_form(request):
	form = StatusForm(request.POST)
	if form.is_valid():
			new_form = form.save()
			return HttpResponseRedirect('/')