from django.apps import AppConfig


class StatusFormConfig(AppConfig):
    name = 'status_form'
